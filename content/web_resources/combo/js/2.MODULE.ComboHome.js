/*
** COMBO HOME
*******/
APP.Modules.ComboHome = (function (app, Private, Public) {

    'use strict';

    Private = {
        boxMilliseconds : '.boxMilliseconds',
        boxHours        : '.boxHours',
        boxMinutes      : '.boxMinutes',
        boxSeconds      : '.boxSeconds'
    },
    Public = {
        config : {
            ID      : 'ComboHome',
            NAME    : 'Render',
            ENGAGE  : true
        }
    };

    Public.init = function () {

        /*
        ** SHELFV1
        *******/
        Private.renderShelfV1();

        /*
        ** COUNT DOWN
        *******/
        Private.CountDown();

        return true;
    };

    /*
    ** RENDERIZADOR DO SHELFV1
    *******/
    Private.renderShelfV1 = function () {

        var template    = document.getElementById('shelfv1-template'),
            content     = document.getElementsByClassName('comboRender')[0],
            pattern     = /(R\$)?[\s]*?([^\,\.]*)([\,\.].*)/,
            price       = null,
            i           = null,
            b           = null,
            html        = '';

        if (content && content.getAttribute('data-init') === 'true') {

            if (comboRender.length > 0) {

                for (var index in comboRender) {

                    price = comboRender[index].productPrice.toString().trim();

                    b     = pattern.exec(price)[2];
                    i     = pattern.exec(price)[3].replace('.', ',');

                    comboRender[index].productInfo['b'] = b;
                    comboRender[index].productInfo['i'] = i;

                    html += Mustache.render(template.innerHTML.trim(), comboRender[index]);
                }
                
                content.innerHTML = html;
            }
        }
    };

    /*
    ** COUNT DOWN
    *******/
    Private.CountDown = function () {

        var countDown = UTILS.countDown({ finalDate :  '2017/05/01 23:59:59' });               
        
        if (countDown) {

            $(Private.boxHours).text(countDown.hours.string);
            $(Private.boxMinutes).text(countDown.minutes.string);
            $(Private.boxSeconds).text(countDown.seconds.string);
            $(Private.boxMilliseconds).text(countDown.milliseconds.string);

            setTimeout(Private.CountDown, 100);
        }
    };

    return Public;

}(APP, {}, {}));