var UTILS = (function (app, window, document, Private, Public) {
    
    'use strict';

    Private = {};
    Public  = {};

    /*
    ** GET BY ID
    *******/
    Private.getId               = function (id) {

        return document.getElementById(id);
    };

    /*
    ** INIT FORM
    *******/
    Public.initForm             = function (config) {

        var form                = null,
            formSubmit          = null,
            formAction          = null,
            formMethod          = null,
            formTarget          = null,
            formCustomSubmit    = null,
            formFields          = null,
            formMessage         = null,

            field               = null,
            fieldId             = null,
            fieldAttribute      = null,
            fieldType           = null,
            fieldMask           = null,
            fieldMax            = null,
            fieldMin            = null;

        if (config) {

            form                = config.form;
            formSubmit          = config.submit;
            formAction          = config.action;
            formMethod          = config.method;
            formTarget          = config.target;
            formCustomSubmit    = config.customSubmit;

            formFields          = config.fields;

            form                = Private.getId(form);
            formSubmit          = Private.getId(formSubmit);

            if (!form) {

                return false;
            }

            /* 
            ** NÃO PERMITE QUE O FORMULARIO SEJA INSTANCIADO MAIS DE UMA VEZ
            *******/
            if(form.getAttribute('data-init')) {

                return false;
            }

            /* 
            ** SE NÃO EXISTIR UM DATA INIT, É ADICIONADO AO FORMULARIO
            *******/
            form.setAttribute('data-init', true);

            /* 
            ** VERIFICANDO É NECESSARIO CONFIGURAR ACTION
            *******/
            if (formAction) {

                if (!form.getAttribute('action')) {

                    form.setAttribute('action', formAction);
                }
            }

            /* 
            ** VERIFICANDO É NECESSARIO CONFIGURAR METHOD
            *******/
            if (formMethod) {
                
                if (!form.getAttribute('method')) {

                    form.setAttribute('method', formMethod);
                }
            }

            /* 
            ** VERIFICANDO É NECESSARIO CONFIGURAR TARGET
            *******/
            if (formTarget) {
                
                if (!form.getAttribute('target')) {

                    form.setAttribute('target', formTarget);
                }
            }

            /* 
            ** CONFIGURANDO CAMPOS DO FORMULARIO
            *******/
            for(var index in formFields) {

                field          = formFields[index];
                fieldId        = form.elements[field.id];
                
                fieldAttribute = field.attributes;
                fieldType      = field.type;
                fieldMask      = field.mask;

                /* 
                ** INSTANCIANDO ATRIBUTOS
                *******/
                if (fieldAttribute) {

                    /* 
                    ** VALIDANDO ATRIBUTOS
                    *******/
                    for (var attribute in fieldAttribute) {

                        if (!fieldId.getAttribute(attribute)) {

                            fieldId.setAttribute(attribute, fieldAttribute[attribute]);
                        }
                    }

                    /* 
                    ** VALIDANDO LIMITE DOS CAMPOS
                    *******/
                    if (fieldAttribute.min && fieldAttribute.max && !fieldMask) {

                        fieldId.addEventListener('keypress', Private.valLimitField, false);
                        fieldId.addEventListener('paste', Private.valLimitField, false);
                    }
                }

                /* 
                ** VALIDANDO TIPAGEM DOS CAMPOS
                *******/
                if (fieldType && !fieldMask) {

                    if (fieldType === 'numeric' || fieldType === 'cpf' || fieldType === 'date') {

                        fieldId.addEventListener('keypress', Private.valTypeFieldNumeric, false);
                        fieldId.addEventListener('paste', Private.valTypeFieldNumeric, false);
                    }
                }

                /*
                ** VALIDANDO MASCARA DOS CAMPOS
                *******/

                if (fieldMask) {

                    if (typeof fieldMask === 'object') {

                        var mask = formFields[index].mask;

                        $(fieldId).mask(mask.maskBehavior, mask.options);

                    } else {

                        $(fieldId).mask(fieldMask);
                    }
                }
            }

            formMessage    = document.createElement('div');
            formMessage.id = 'formMessage';

            form.appendChild(formMessage);

            /* 
            ** ENVIANDO FORMULARIO
            *******/
            formSubmit.addEventListener('click', Private.submitForm, false);
            formSubmit.config = config;
        }

        return false;
    };

    /*
    ** CONFIGURANDO ENVIO
    *******/
    Private.submitForm          = function (event) {

        var config              = event.target.config,
            form                = null,
            formAjax            = null,
            formAction          = null,
            formMethod          = null,
            formCustomSubmit    = null;


        if (Private.valForm(config)) {

            if (!Private.valCustomValidation(config)) {

                return event.preventDefault ? event.preventDefault() : event.returnValue = false;
            }

            form                = config.form;
            form                = Private.getId(form);
            formAction          = config.action;
            formMethod          = config.method;
            formCustomSubmit    = config.customSubmit;
            formAjax            = config.ajax;

            if (formAjax) {

                if (formAjax.actived) {
                    
                    /*
                    ** NAO UTILIZAR URL, NO PARAMETROS DA CONFIGURAÇAO DO AJAX
                    ** CASO HAJA O PARAMETRO URL, ELE SERÁ SUBSTITUIDO PELA VARIAVEL "formAction"
                    *******/
                    formAjax.config.url = (formAjax.config.url || formAction);

                    /*
                    ** NAO UTILIZAR TYPE, NO PARAMETROS DA CONFIGURAÇAO DO AJAX
                    ** CASO HAJA O PARAMETRO TYPE, ELE SERÁ SUBSTITUIDO PELA VARIAVEL "formMethod"
                    *******/
                    formAjax.config.type = (formAjax.config.type || formMethod);
                    
                    formAjax.config.data = Public.serialize({
                        form      : config.form,
                        json      : true,
                        stringify : true
                    });

                    /*
                    ** ENVIANDO FORMULARIO VIA AJAX
                    *******/
                    $.ajax(formAjax.config);
                } else {

                    /*
                    ** ENVIANDO FORMULARIO SEM O USO DO AJAX
                    *******/
                    form.submit();
                }
            } else {

                if (formCustomSubmit) {

                    /*
                    ** ENVIO CUSTOMIZADO
                    *******/
                    formCustomSubmit();
                } else {

                    /*
                    ** ENVIANDO FORMULARIO SEM O USO DO AJAX
                    *******/
                    form.submit();
                }
            }
        }

        return event.preventDefault ? event.preventDefault() : event.returnValue = false;
    };

    /*
    ** VALIDANDO CAMPOS DO FORMULARIO
    *******/
    Private.valForm             = function (config) {

        var form            = null,
            formFields      = null,
            formMessage     = null,

            field           = null,
            fieldId         = null,
            fieldType       = null,
            fieldRequire    = null,
            fieldAttribute  = null,
            fieldMessage    = null,
            fieldLength     = null,
            fieldValue      = null,
            fieldMax        = null,
            fieldMin        = null,

            error           = false,
            patternOk       = null,

            ul              = document.createElement('ul'),
            li              = null;

        form        = config.form;
        form        = Private.getId(form);
        formFields  = config.fields;

        for(var index in formFields) {

            field          = formFields[index];
            fieldId        = form.elements[field.id];
            
            fieldAttribute = field.attributes;
            fieldType      = field.type;
            fieldRequire   = field.require;

            if (typeof field === 'object') {

                if (fieldId.length !== 0 && !fieldId.disabled) {

                    fieldValue  = fieldId.value;
                    fieldLength = fieldValue.length;
                }

                fieldId.className = '';

                /*
                ** VALIDANDO CAMPOS VAZIOS
                *******/
                if (fieldId && fieldLength <= 0 && !fieldId.disabled && fieldRequire) {

                    fieldMessage = field.message;

                    fieldId.className = 'field-error';

                    li           = document.createElement('li');
                    li.innerHTML = 'O campo ' + fieldMessage + ' não pode estar vázio';

                    ul.appendChild(li);

                    error = true;
                }

                /*
                ** VALIDANDO TIPO DOS CAMPOS
                *******/
                if (fieldType) {

                    if (fieldLength > 0 && !fieldId.disabled) {

                        fieldMessage = field.message;

                        if (fieldType === 'numeric') {

                            patternOk = (/^([\d|\.|\,|\)|\(|\-|\/|\s]+)+$/).test(fieldValue);

                            if (!patternOk) {

                                fieldId.className = 'field-error';

                                li           = document.createElement('li');
                                li.innerHTML = 'O campo ' +  fieldMessage + ' não está em um formato válido!';

                                ul.appendChild(li);

                                error = true;
                            }
                        } else if (fieldType === 'email') {

                            patternOk = Public.valTypeFieldEmail(fieldValue);

                            if (!patternOk) {

                                fieldId.className = 'field-error';

                                li           = document.createElement('li');
                                li.innerHTML = 'O Email não está em um formato válido!';

                                ul.appendChild(li);

                                error = true;
                            }
                        } else if (fieldType === 'cpf') {

                            patternOk = Private.valTypeFieldCpf(fieldValue);

                            if (!patternOk) {

                                fieldId.className = 'field-error';

                                li           = document.createElement('li');
                                li.innerHTML = 'O CPF não está em um formato válido!';

                                ul.appendChild(li);

                                error = true;
                            }
                        } else if (fieldType === 'date') {

                            patternOk = Private.valTypeFieldDate(fieldValue);

                            if (!patternOk) {

                                fieldId.className = 'field-error';

                                li           = document.createElement('li');
                                li.innerHTML = 'O Data não está em um formato válido!';

                                ul.appendChild(li);

                                error = true;
                            }
                        }
                    }
                }

                /*
                ** VALIDANDO TAMANHO DOS CAMPOS
                *******/
                if (fieldAttribute) {

                    fieldMax = fieldAttribute.max;
                    fieldMin = fieldAttribute.min;

                    if (fieldMin) {

                        if (fieldLength < fieldMin && fieldLength > 0 && !fieldId.disabled) {

                            fieldId.className = 'field-error';

                            li           = document.createElement('li');
                            li.innerHTML = 'Preencha o valor mínimo de ' + fieldMin + ' caracteres';

                            ul.appendChild(li);

                            error = true;
                        }
                    }

                    if (fieldMax) {

                        if (fieldLength > fieldMax && fieldLength > 0 && !fieldId.disabled) {

                            fieldId.className = 'field-error';

                            li           = document.createElement('li');
                            li.innerHTML = 'O valor máximo de ' + fieldMax + ' caracteres foi ultrapassado';

                            ul.appendChild(li);

                            error = true;
                        }
                    }
                }
            }
        }

        formMessage = form.lastChild;
        formMessage.style.display = 'none';

        formMessage.innerHTML = ul.outerHTML;

        if (error) {

            formMessage.innerHTML   = ul.outerHTML;
            formMessage.style.display = 'block';

            return false;
        }

        return true;
    };

    /*
    ** VALIDANDO FUNÇOES CUSTOMIZADAS
    *******/
    Private.valCustomValidation = function (config) {

        var customValidation = config.customValidation,
            returnValidation = false;

        if (!customValidation) {

            return true;
        }

        $.each(customValidation, function (key, value) {

            returnValidation = value();
        });

        return returnValidation;
    };

    /* 
    ** VALIDANDO LIMITE DOS CAMPOS
    *******/
    Private.valLimitField       = function (event) {

        var field           = event.target,
            fieldKeyCode    = null,
            fieldValue      = null,
            fieldLength     = null,
            fieldMax        = parseInt(field.getAttribute('max'), 10);

        if (event.type === 'paste') {

            field       = event.clipboardData || window.clipboardData;
            fieldValue  = field.getData('Text');

            fieldLength = fieldValue.length;

            if (fieldLength >= fieldMax) {

                fieldValue = fieldValue.substring(0, fieldMax);

                field       = event.target;
                field.value = fieldValue;

                return event.preventDefault ? event.preventDefault() : event.returnValue = false;
            }
        } else {

            if (field.selectionStart === 0 && field.selectionEnd === fieldMax) {

                field.value = '';
            }

            fieldKeyCode    = event.keyCode || event.which;
            fieldValue      = field.value;
            fieldLength     = fieldValue.length;

            if (fieldKeyCode !== 8) {

                if (fieldLength >= fieldMax) {

                    field.value = fieldValue;

                    return event.preventDefault ? event.preventDefault() : event.returnValue = false;
                }
            }
        }
    };

    /* 
    ** VALIDANDO CAMPOS NUMERICOS
    *******/
    Private.valTypeFieldNumeric = function (event) { 

        var field           = event.target,
            fieldKeyCode    = null,
            fieldValue      = null,
            pattern         = /([\d|.|,|\)|\(|\-|\/]+)+/,
            patternOk       = true;

        if (event.type === 'paste') {

            field       = event.clipboardData || window.clipboardData;
            fieldValue  = field.getData('Text');
            patternOk   = pattern.test(fieldValue);
        } else {

            fieldKeyCode = event.keyCode || event.which;
            
            if (fieldKeyCode !== 8) {

                fieldValue   = String.fromCharCode(fieldKeyCode);
                patternOk    = pattern.test(fieldValue);
            }
        }

        if (!patternOk) {

            return event.preventDefault ? event.preventDefault() : event.returnValue = false;
        }
    };

    /*
    ** VALIDANDO CAMPOS DE EMAIL
    *******/
    Public.valTypeFieldEmail    = function (value) { 

        var fieldValue = value,
            patternOk  = (/^([\w\-]+(?:\.[\w\-]+)*)@((?:[\w\-]+\.)*\w[\w\-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i).test(fieldValue);

        return patternOk;
    };

    /*
    ** VALIDANDO CPF
    *******/
    Private.valTypeFieldCpf     = function (value) {

        var cpf = value.replace(/[.|\-]/g, ''),
            numbers,
            digits,
            sum,
            i,
            result,
            digits_equals = 1;

        if (cpf.length < 11) {

            return false;
        }

        for (i = 0; i < cpf.length - 1; i = i + 1) {

            if (cpf.charAt(i) !== cpf.charAt(i + 1)) {

                digits_equals = 0;

                break;
            }
        }

        if (!digits_equals) {

            numbers = cpf.substring(0, 9);
            digits  = cpf.substring(9);
            sum     = 0;

            for (i = 10; i > 1; i = i - 1) {

                sum += numbers.charAt(10 - i) * i;
            }

            result = sum % 11 < 2 ? 0 : 11 - sum % 11;

            if (parseInt(result, 10) !== parseInt(digits.charAt(0), 10)) {

                return false;
            }

            numbers = cpf.substring(0, 10);
            sum = 0;

            for (i = 11; i > 1; i = i - 1) {
                sum += numbers.charAt(11 - i) * i;
            }

            result = sum % 11 < 2 ? 0 : 11 - sum % 11;

            if (parseInt(result, 10) !== parseInt(digits.charAt(1), 10)) {

                return false;
            }

            return true;
        }

        return false;
    };

    /*
    ** VALIDANDO DATA
    *******/
    Private.valTypeFieldDate    = function (value) {

        var fieldValue = value,
            patternOk  = (/^((?:0?[0-9])|(?:[1-2][0-9])|(?:3[01]))\/((?:0?[0-9])|(?:1[0-2]))\/([0-9]{4})?$/i).test(fieldValue);

        return patternOk;
    };

    /*
    ** MASCARA SEM JQUERY
    *******/
    Public.mask                 = function () {

        return {

            pattern1     : /[\d]+/g,
            pattern2     : /[\W]+/g,
            pattern3     : /(\W|\d+)/g,

            arrPattern   : [],
            arrReplace   : [],

            newPattern   : '',
            newReplace   : '',

            params         : event ? event.target.params : arguments[0],

            fieldMask      : null,
            maskLength     : null,

            characters     : null,
            charsLength    : 0,

            selectionStart : null,
            selectionEnd   : null,

            countReplace   : 1,
            field          : null,
            fieldKeyCode   : null,
            fieldValue     : null,
            regex          : null,

            init       : function () {

                /*
                ** RECUPERANDO DADOS DA MASCARA
                *******/
                this.fieldMask  = this.params.mask;
                this.maskLength = this.fieldMask.length;
                this.characters = this.fieldMask.match(this.pattern3);

                /*
                ** IDENTIFICANDO ELEMENTO
                *******/
                this.field = document.getElementById(this.params.id) || document.getElementsByClassName(this.params.id)[0];

                /*
                ** ADICIONANDO EVENTO AO CAMPO
                *******/
                this.field.addEventListener('keypress', this.keypress, false);
                this.field.addEventListener('paste', this.paste, false);
                this.field.self = this;

                return this;
            },
            mask       : function () {

                /*
                ** GERANDO NOVO REGEXP
                *******/
                for(var chars in this.characters) {

                    this.regex = new RegExp(this.pattern1);

                    if (this.regex.test(this.characters[chars])) {

                        this.charsLength = this.characters[chars].length;

                        this.arrPattern[chars] = (chars <= 0 ? '^' : '') + '([\\d]{' + this.charsLength  + '})';

                        this.arrReplace[chars] = '$' + this.countReplace;

                        this.countReplace++;
                    }

                    this.regex = new RegExp(this.pattern2);

                    if (this.regex.test(this.characters[chars])) {

                        this.arrPattern[chars] = (chars <= 0 ? '^' : '') + '\\' + this.characters[chars].toString().replace(/\s/, '\s') + '?';
                        this.arrReplace[chars] = this.characters[chars];
                    }
                }

                /*
                ** VALIDANDO DADOS DO CAMPO
                *******/
                for(var pattern in this.arrPattern) {

                    if (!isNaN(pattern)) {

                        this.newPattern += this.arrPattern[pattern];
                        this.newReplace += this.arrReplace[pattern];

                        this.regex = new RegExp(this.newPattern, 'gi');

                        if (this.regex.test(this.fieldValue)) {

                            this.field.value = this.fieldValue.replace(this.regex, this.newReplace);
                        }
                    }
                }

                this.newPattern   = '';
                this.newReplace   = '';
                this.countReplace = 1;

                return this;
            },
            keypress   : function () {

                var self  = this.self,
                    start = this.selectionStart,
                    end   = this.selectionEnd;

                self.fieldValue   = this.value.replace(this.pattern2, '');
                self.fieldKeyCode = event.keyCode ? event.keyCode : event.which;

                if (self.fieldKeyCode === 8 || event.ctrlKey) {

                    return false;
                }

                if (self.fieldValue.length >= self.maskLength) {

                    return event.preventDefault ? event.preventDefault() : event.returnValue = false;
                }

                if (this.selectionStart < self.fieldValue.length) {

                    self.fieldValue = self.fieldValue.substring(0, end) + 
                                        String.fromCharCode(self.fieldKeyCode) + 
                                            self.fieldValue.substring(start, self.fieldValue.length);

                    return event.preventDefault ? event.preventDefault() : event.returnValue = false;

                    //this.setSelectionRange(self.selectionStart, self.selectionEnd);
                }

                self.mask();

                return this;
            },
            paste      : function () {

                var fieldSelf = this,
                    self      = this.self;

                setTimeout(function () {

                    self.fieldValue = fieldSelf.value.substr(0, self.fieldMask.replace(/[\W]/g, '').length);

                    self.mask();

                }, 100);

                return this;
            }      
        }.init();
    };

     /*
    ** SLIDER TOUCH
    *******/
    Public.touchSlider          = function () {

        var touchMoveX      = 0,
            touchStartX     = 0,
            touchMoveY      = 0,
            touchStartY     = 0,

            touchContent    = null,
            touchWidth      = null,

            transform       = null,
            distance        = null,

            move            = false,

            moveX           = 0,
            moveY           = 0,

            allowedTime     = 300,
            startTime       = 0,

            params          = arguments[0],

            speed           = null,
            duration        = null,
            auto            = null,
            navigator       = null,
            nav             = null,
            prev            = null,
            next            = null,

            slider          = null,
            sliderWidth     = null,
            sliderHeight    = null,
            sliders         = null,
            sliderFirst     = null,
            sliderLast      = null,
            sliderLastClone = null,

            newHTML         = null,
            oldHTML         = null,
            initialHTML     = null,

            bullet          = null,
            timeout         = null,

            self            = null;

        return {

            init                : function () {

                self = this;

                try {

                    if (params) {

                        speed       = params.speed      !== undefined ? params.speed    : 600;
                        duration    = params.duration   !== undefined ? params.duration : 4000;
                        auto        = params.auto       !== undefined ? params.auto     : true;

                        nav         = document.getElementById(params.nav)   || document.getElementsByClassName(params.nav)[0];
                        prev        = document.getElementById(params.prev)  || document.getElementsByClassName(params.prev)[0];
                        next        = document.getElementById(params.next)  || document.getElementsByClassName(params.next)[0];
                        slider      = document.getElementById(params.id)    || document.getElementsByClassName(params.id)[0];

                        oldHTML     = slider.outerHTML;
                    }

                    if (self.instance()) {

                        self.sliderEvents();
                    }

                    return this;
                } catch (err) {

                    console.log(err);
                }     
            },
            instance            : function () {

                var sliderHTML      = null,
                    sliders         = self.removeChild(slider),
                    slidersLength   = sliders.length,
                    slidersWidth    = null;

                if (sliders.length <= 1 || !slider) {

                    slider.style.height = 'auto';

                    return false;
                }

                /*
                ** CUSTOMIZANDO SLIDER
                *******/
                slider.style.cssText    = 'overflow: hidden; height: 0;';

                /*
                ** CUSTOMIZANDO SLIDERS
                *******/
                slidersWidth = (100 / (slidersLength + 1));

                for (var index in sliders) {

                    if (typeof sliders[index] === 'object') {

                        sliders[index].className = index;

                        sliders[index].style.cssText = 
                            'display        : inline-block; ' + 
                            'vertical-align : middle;';

                        sliders[index].style.width = Math.abs(slidersWidth) + '%';
                    }
                }

                initialHTML = slider.innerHTML;
                sliderHTML  = slider.innerHTML;

                /*
                ** CUSTOMIZANDO TOUCH CONTENT
                *******/
                touchWidth  = (100 * (slidersLength + 1)) + '%';

                touchContent    = document.createElement('div');
                touchContent.style.cssText = 
                    'width      : ' + touchWidth + '; ' + 
                    'height     : auto; ' +
                    'text-align : left;';

                touchContent.className = 'touchContent';
                touchContent.innerHTML = sliderHTML;


                /*
                ** JOGANDO CONTEUDO INCIAL NO NOVO CONTENT
                *******/
                slider.innerHTML = '';
                slider.appendChild(touchContent);
                
                slider.style.height = 'auto';

                self.sliderStart();

                /*
                ** ADICIONANDO NAVIGATOR DE NAVEGAÇAO
                *******/
                if (nav) {

                    for (var count = 0; count < slidersLength; count++) {

                        bullet              = document.createElement('button');
                        bullet.value        = count;
                        bullet.className    = 'navigator';
                        bullet.innerHTML    = document.createElement('span').outerHTML;

                        nav.appendChild(bullet);
                    }

                    navigator = document.getElementsByClassName('navigator');

                    navigator[0].className += ' actived';
                }

                return true;
            },
            touchStart          : function (event) {

                self.sliderData();

                startTime = new Date().getTime();

                sliders.insertBefore(sliderLastClone, sliderFirst);

                distance  = (sliderWidth * -1);
                transform = 'translateX(' + distance + 'px)';

                touchContent.style.transform = transform;

                touchStartX = event.changedTouches[0].pageX;
                touchStartY = event.changedTouches[0].pageY;

                self.sliderStop();

                slider.addEventListener('touchmove', self.touchMove, false);
                slider.addEventListener('touchend', self.touchEnd, false);
            },
            touchMove           : function (event) {

                self.sliderData();

                move        = true;

                touchMoveX  = (event.changedTouches[0].pageX);
                touchMoveY  = (event.changedTouches[0].pageY);

                moveX       = ((touchStartX - touchMoveX) * -1);
                moveY       = ((touchStartY - touchMoveY) * -1);

                distance    = ((sliderWidth * -1) + moveX);
                transform   = 'translateX(' + distance + 'px)';

                touchContent.style.transform = transform;
            },
            touchEnd            : function (event) {

                var touchEndX   = (event.changedTouches[0].pageX - touchStartX),
                    touchEndY   = (event.changedTouches[0].pageY - touchStartY),

                    directionX  = (touchEndX) < 0 ? 'left' : 'right',
                    directionY  = (touchEndY) < 0 ? 'up' : 'down',

                    elapsedTime = (new Date().getTime() - startTime);

                self.sliderData();
                self.sliderEvents(true);

                if (move) {

                    if (directionX === 'left') {

                        if ((elapsedTime <= allowedTime) || (moveX * -1) >= (sliderWidth / 2)) {

                            self.scrollLeftByTouch();
                        } else {

                            self.scrollEffect((sliderWidth * -1), speed, function () {

                                sliders.removeChild(sliderFirst);
                            });
                        }
                    } else if (directionX === 'right') {

                        if ((elapsedTime <= allowedTime) || moveX >= (sliderWidth / 2)) {
                            
                            self.scrollRightByTouch();

                        } else {

                            self.scrollEffect((sliderWidth * -1), speed, function () {

                                sliders.removeChild(sliderFirst);
                            });
                        }
                    }
                } else {

                    sliders.removeChild(sliderFirst);

                    transform = 'translateX(0)';

                    touchContent.style.transform = transform;

                    self.sliderEvents();
                }
            },
            scrollLeftByTouch   : function (event) {

                self.sliderData();

                distance = (sliderWidth * -2);

                self.scrollEffect(distance, speed, function () {

                    sliders.removeChild(sliderFirst);

                    self.sliderData();

                    sliders.insertBefore(sliderFirst, sliderLast.nextSibling);
                });

                return false;
            },
            scrollRightByTouch  : function (event) {

                self.sliderData();

                self.scrollEffect(0, speed, function () {

                    sliders.removeChild(sliderLast);
                });

                return false;
            },
            scrollLeftByEvent   : function (event) {

                self.sliderData();

                self.sliderEvents(true);

                distance = (sliderWidth * -1);

                self.scrollEffect(distance, speed, function () {

                    self.sliderData();

                    sliders.insertBefore(sliderFirst, sliderLast.nextSibling);
                });

                return false;
            },
            scrollRightByEvent  : function (event) {

                self.sliderData();

                self.sliderEvents(true);
                
                sliders.insertBefore(sliderLastClone, sliderFirst);

                distance  = (sliderWidth * -1);
                transform = 'translateX(' + distance + 'px)';

                touchContent.style.transform = transform;

                self.scrollEffect(0, speed, function () {

                    sliders.removeChild(sliderLast);
                });

                return false;
            },
            scrollNavigator     : function (event) {

                var nextHTML        = [],
                    prevHTML        = [],
                    newHTML         = null,
                    newSliderHTML   = null;

                try {

                    if (this.classList.contains('actived')) {

                        return false;
                    }

                    self.sliderData();
                    self.sliderEvents(true);

                    newSliderHTML           = document.createElement('div');
                    newSliderHTML.innerHTML = initialHTML;
                    
                    for (var count = 0; count < this.value; count++) {

                        nextHTML.push(newSliderHTML.childNodes[count]);
                    }

                    for (var count2 = this.value; count2 < newSliderHTML.childNodes.length; count2++) {

                        prevHTML.push(newSliderHTML.childNodes[count2]);
                    }

                    while (touchContent.childNodes.length !== 1) {

                        touchContent.removeChild(touchContent.lastChild);
                    }

                    newHTML = prevHTML.concat(nextHTML);
                    
                    for (var index in newHTML) {

                        if (newHTML[index].nodeType === 1) {

                            touchContent.appendChild(newHTML[index]);
                        }
                    }

                    sliderWidth = (sliderWidth * -1);

                    self.scrollEffect(sliderWidth, speed, function () {

                        touchContent.removeChild(sliderFirst);
                    });
                } catch (err) {

                    console.log('nav err: ', err);
                }

                return false;
            },
            scrollEffect        : function (to, duration, callback) {

                var newCallback     = function () {

                        if (callback) {

                            callback();
                        }

                        move = false;

                        touchContent.style.transform  = 'none';
                        touchContent.style.transition = 'none';

                        self.sliderStart();
                        self.sliderEvents();
                        self.setNavigator();

                        touchContent.removeEventListener('transitionend', newCallback, false);
                    },
                    transition          = function () {

                        touchContent.style.transitionProperty       = 'transform';
                        touchContent.style.transitionTimingFunction = 'ease';
                        touchContent.style.transitionDuration       = (duration / 1000) + 's';

                        transform = 'translateX(' + to + 'px)';

                        touchContent.style.transform = transform;
                    };

                if (move) {

                    transition();
                } else {

                    setTimeout(transition, 100);
                }

                touchContent.addEventListener('transitionend', newCallback, false);
            },
            setNavigator        : function () {

                var navigatorIndex = 0;

                if (nav) {

                    self.sliderData();

                    for (var index in navigator) {
                        
                        if (navigator[index].nodeType === 1) {

                            navigator[index].classList.remove('actived');
                        }
                    }

                    navigatorIndex = parseInt(sliderFirst.className, 10);

                    navigator[navigatorIndex].className += ' actived';
                }
            },
            sliderData          : function () {

                sliders         = slider.getElementsByTagName('div')[0];

                sliderFirst     = sliders.firstChild;
                sliderLast      = sliders.lastChild;
                sliderLastClone = sliders.lastChild.cloneNode(true);

                sliderWidth     = slider.offsetWidth;
                sliderHeight    = slider.offsetHeight;
            },
            sliderStart         : function () {

                if (auto) {

                    self.sliderStop();

                    timeout = setTimeout(self.scrollLeftByEvent, (duration + speed));
                }
            },
            sliderStop          : function () {

                clearTimeout(timeout);
            },
            sliderEvents        : function (remove) {

                if (remove) {

                    slider.removeEventListener('touchstart', self.touchStart, false);
                    slider.removeEventListener('touchmove', self.touchMove, false);
                    slider.removeEventListener('touchend', self.touchEnd, false);

                    if (prev && next) {

                        prev.removeEventListener('click', self.scrollRightByEvent, false);
                        next.removeEventListener('click', self.scrollLeftByEvent, false);
                    }

                    if (nav) {

                        for (var index in navigator) {
                        
                            if (navigator[index].nodeType === 1) {

                                navigator[index].removeEventListener('click', self.setNavigator, false);
                                navigator[index].removeEventListener('click', self.scrollNavigator, false);
                            }
                        }
                    }

                    return false;
                }

                slider.addEventListener('touchstart', self.touchStart, false);
                /*slider.addEventListener('touchmove', self.touchMove, false);
                slider.addEventListener('touchend', self.touchEnd, false);*/

                if (prev && next) {

                    prev.addEventListener('click', self.scrollRightByEvent, false);
                    next.addEventListener('click', self.scrollLeftByEvent, false);
                }

                window.addEventListener('focus', self.sliderStart, false);
                window.addEventListener('blur', self.sliderStop, false);

                if (nav) {

                    for (var index2 in navigator) {
                        
                        if (navigator[index2].nodeType === 1) {

                            navigator[index2].addEventListener('click', self.setNavigator, false);
                            navigator[index2].addEventListener('click', self.scrollNavigator, false);
                        }
                    }
                }
            },
            sliderReset         : function () {

                /*
                ** DESTRUINDO O SLIDER
                *******/
                newHTML             = document.createElement('div');
                newHTML.innerHTML   = oldHTML;

                slider.parentNode.replaceChild(newHTML.firstChild, slider);

                self.init();
            },
            removeChild         : function (node) {

                var nodes = node.childNodes;

                for (var index in nodes) {

                    if (nodes[index]) {

                        if (typeof nodes[index] === 'function') {

                            delete nodes[index];
                        } else if (nodes[index].nodeType !== 1 && nodes[index].nodeType !== undefined) {

                            node.removeChild(node.childNodes[index]);
                        }
                    }
                }

                return nodes;
            }
        }.init();
    };

    /*
    ** CONTADOR REGRESSIVO
    *******/
    Public.countDown            = function () {

        var finalDate       = arguments[0].finalDate || null,
            result          = {},

            arrWeekDay      = [
                'Domingo',
                'Segunda',
                'Terça',
                'Quarta',
                'Quinta',
                'Sexta',
                'Sábado'
            ],
            patternDate     = /^([\d]{2})\/([\d]{2})\/([\d]{4})[\s]?(.*)$/,
            date            = null,
            weekDay         = null,

            week            = null,
            day             = null,
            month           = null,
            year            = null,

            timestamp       = null,

            time            = null,
            days            = null,
            hours           = null,
            minutes         = null,
            seconds         = null,
            milliseconds    = null,

            actualDate      = null,
            actualHour      = null,

            newDate         = null;

        return {

            init : function () {

                this.date();

                return result;
            },
            date : function () {

                /*
                ** CALCULANDO DIAS RESTANTES
                *******/
                if (patternDate.test(finalDate)) {

                    finalDate = finalDate.replace(patternDate, '$3/$2/$1 $4');
                }

                actualDate      = new Date();
                finalDate       = new Date(finalDate);

                newDate         = (finalDate.getTime() - actualDate.getTime()) / 1000;

                week            = Math.floor(newDate / 604800);
                days            = Math.floor(newDate / 86400);
                hours           = Math.floor(newDate / 3600) % 24;
                minutes         = Math.floor(newDate / 60) % 60;
                seconds         = Math.floor(newDate) % 60;
                milliseconds    = newDate.toFixed(2).replace(/([^\.]*\.)/, '');
                milliseconds    = parseInt(milliseconds, 10);

                weekDay         = arrWeekDay[finalDate.getDay()];
                weekDay         = (days <= 0 && actualDate.getDay() === finalDate.getDay()) ? 'Hoje' : weekDay;

                finalDate.setHours(finalDate.getHours() + Math.round(finalDate.getMinutes() / 60));
                finalDate.setMinutes(0);

                if (week > 0 || days > 0 || hours > 0 || minutes > 0 || seconds > 0) {

                    result = {
                        weekDay         : weekDay,
                        lastHours       : finalDate.getHours(),
                        week            : {
                            string : (week < 10 ? '0' + week : week).toString(),
                            int    : (week > 0  ? week       : 0)
                        },
                        days            : {
                            string : (days < 10 ? '0' + days : days).toString(),
                            int    : (days > 0  ? days       : 0)
                        },
                        hours           : {
                            string : (hours < 10 ? '0' + hours : hours).toString(),
                            int    : (hours > 0  ? hours       : 0)
                        },
                        minutes         : {
                            string : (minutes < 10 ? '0' + minutes : minutes).toString(),
                            int    : (minutes > 0  ? minutes       : 0)
                        },
                        seconds         : {
                            string : (seconds < 10 ? '0' + seconds : seconds).toString(),
                            int    : (seconds > 0  ? seconds       : 0)
                        },
                        milliseconds    : {
                            string : (milliseconds < 10 ? '0' + milliseconds : milliseconds).toString(),
                            int    : (milliseconds > 0  ? milliseconds       : 0)
                        }
                    };
                } else {

                    result = false;
                }

                return this;
            }
        }.init();
    };

    /*
    ** INSERINDO PLACEHOLDER PARA OS NAVEGADORES INFERIORES AO INTERNET EXPLORER 10
    *******/
    /*Private.internetExplorerPlaceholder = function (formId, config) {

        var fieldId,
            placeholder,
            range;

        if (config.length > 0) {

            $.each(config[0].fields, function (cKey, cValue) {

                fieldId = formId.find('#' + cValue.id);

                fieldId.val(fieldId.attr('placeholder'));

                fieldId.on({
                    'focus' : function () {

                        placeholder = $(this).attr('placeholder');

                        if ($(this).val() === placeholder) {

                            if ($(this).get(0).createTextRange) {

                                range = $(this).get(0).createTextRange();

                                range.collapse(true);
                                range.moveEnd('character', 0);
                                range.moveStart('character', 0);
                                range.select();
                            }
                        }
                    },
                    'blur' : function () {

                        placeholder = $(this).attr('placeholder');

                        if ($(this).val().length <= 0) {

                            $(this).val(placeholder);
                        }
                    },
                    'keydown' : function (event) {

                        placeholder = $(this).attr('placeholder');

                        if ($(this).val() === placeholder && event.keyCode !== 17) {

                            $(this).val('');
                        }
                    }
                }, fieldId);
            });
        }
    };*/

    /*
    ** SERIALIZE DE FORMULARIO
    ******/
    Public.serialize            = function () {

        var params      = arguments[0],
            form        = (params.form        || null),
            json        = (params.json        || null),
            stringify   = (params.stringify   || null),

            formFields  = [],

            fieldName   = null,
            fieldValue  = null,

            dataObject  = {},
            dataString  = '';

        return {
            init : function () {

                form = Private.getId(form);

                if (!form || form.nodeName !== "FORM") {

                    return;
                }

                formFields = form.elements;

                for(var index = 0; index <= formFields.length; index++) {

                    if (formFields[index]) {

                        if (formFields[index].name) {

                            if (formFields[index].nodeName === 'INPUT' || formFields[index].nodeName === 'TEXTAREA' || formFields[index].nodeName === 'SELECT') {

                                if (formFields[index].type !== 'submit' && formFields[index].type !== 'reset' && formFields[index].type !== 'button') {

                                    if (formFields[index].type === 'checkbox' || formFields[index].type === 'radio') {

                                        if (!formFields[index].checked) {

                                            continue;
                                        }
                                    } 

                                    fieldName  = formFields[index].name;
                                    fieldValue = json ? formFields[index].value : encodeURIComponent(formFields[index].value);

                                    if (json) {

                                        dataObject[fieldName] = fieldValue;
                                    } else {

                                        dataString += fieldName + '=' + fieldValue;

                                        if (index < (formFields.length - 1)) {

                                            dataString += '&';
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (json) {

                    if (stringify) {

                        return JSON.stringify(dataObject);
                    }

                    return dataObject;
                } else {

                    return dataString;
                }
            }
        }.init();
    };

    /*
    ** RECUPERANDO PARAMETROS DA URL
    *******/
    Public.getParam             = function (config) {

        var paramters       = window.location.search.substring(1),
            paramtersArray  = paramters.split('&'),
            param           = config ? config.param : null,
            type            = config ? config.type : null,
            keys            = null,
            values          = null,
            response        = config ? {} : paramters;

        if (config) {

            for (var index in paramtersArray) {

                values  = paramtersArray[index].split('=');

                if (response.hasOwnProperty(values[0])) {

                    response[values[0]] = typeof response[values[0]] === 'object' ? response[values[0]] : [response[values[0]]];
                    response[values[0]].push(values[1]);
                } else {

                    response[values[0]] = values[1];
                }
            }

            if (param !== '' && param !== null && param !== undefined) {

                return response[param] || false;
            }

            if (type === 'string') {

                return paramters || false;
            }
        }

        return response;
    };

    return Public;

}(APP, window, document, {}, {}));