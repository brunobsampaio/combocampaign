var APP = (function () {
    'use strict';

    var 
        Privates = {},
        Modules  = {},
        Debug    = null,
        Init     = null;

    /*
    ** PROTOTYPES
    *******/
    if (!Array.prototype.indexOf) {

        Array.prototype.indexOf = function(obj, start) {

            for (var i = (start || 0), j = this.length; i < j; i++) {

                if (this[i] === obj) {

                    return i;
                }
            }

            return -1;
        };
    }

    if (!Element.prototype.addEventListener) {

        Element.prototype.addEventListener = function (event, method) {

            this['on' + event] = method;
            
            return this['on' + event];
        };
    }

    if (!document.getElementsByClassName) {

        document.getElementsByClassName = function (element) {

            return this.querySelectorAll('.' + element);
        };
    }

    /*
    ** INICIALIZADOR DE MODULOS
    *******/
    Init = function (config) {

        var id           = null,
            modules      = config.modules,
            errorModules = [];

        /*
        ** VERIFICANDO MODULOS
        *******/
        for(var module in modules) {

            if (modules[module].config.EXTEND) {

                if (modules[module].config.ENGAGE) {

                    if (modules[modules[module].config.EXTEND]) {

                        if (!modules[modules[module].config.EXTEND].config.ENGAGE) {

                            delete modules[module];
                        }
                    } else {

                        delete modules[module];
                    }
                } else {

                    delete modules[module];
                }
            } else if (!modules[module].config.ENGAGE) {

                delete modules[module];
            }
        }

        for(module in modules) {

            id = (/^#{1}/).test(modules[module].config.ID) ? modules[module].config.ID.replace('#', '') : modules[module].config.ID;

            if (document.getElementById(id) || !id) {

                /*try
                {*/
                    if (errorModules.indexOf(modules[module].config.EXTEND) === -1) {

                        modules[module].init();
                    }
                /*}
                catch(error)
                {
                    console.log('error', error);
                    errorModules.push(module);

                    delete modules[module];
                }*/
            }
        }
    };

    /*
    ** DEBUG DO CONSOLE
    *******/
    Debug = function (debug) {

        if (!debug) {

            window.console.log = function () {

                return false;
            };
        }
    };

    return {
        Init    : Init,
        Debug   : Debug,
        Modules : Modules
    };

}());