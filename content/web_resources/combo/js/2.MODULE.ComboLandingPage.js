/*
** COMBO LANDING PAGE
*******/
APP.Modules.ComboLandingPage = (function (app, Private, Public) {

    'use strict';

    Private = {
        button  : null,
        uri     : null
    },
    Public = {
        config : {
            ID      : 'ComboLandingPage',
            NAME    : 'Render',
            ENGAGE  : true
        }
    };

    Public.init = function () {

        /*
        ** COLLECTIONS
        *******/
        Private.renderCollections();

        /*
        ** SCROLL ASIDE
        *******/
        Private.scrollAside();

        /*
        ** SCROLL EVENT
        *******/
        document.addEventListener('scroll', Private.scrollAside, false);

        /*
        ** SCROLL EVENT
        *******/
        $(document).on('click', '#asideMenu ul li a', Private.scrollCollection);

        return true;
    };

    /*
    ** SCROLL ASIDE
    *******/
    Private.scrollAside = function (event) {

        var asideFixed          = document.getElementsByClassName('asideFixed')[0],
            asideBanner         = document.getElementsByClassName('asideBanner')[0],
            asideMenu           = document.getElementById('asideMenu'),
            comboLandingPage    = document.getElementById('comboLandingPage'),
            asideffsetTop       = asideFixed.offsetTop,
            windowOffsetTop     = window.pageYOffset,
            margin              = 32,

            contentHeight       = comboLandingPage.clientHeight,
            menuHeight          = asideMenu.clientHeight,
            bannerHeight        = asideBanner.clientHeight;

        if ((windowOffsetTop - margin) >= asideffsetTop && (windowOffsetTop - margin) <= (contentHeight - menuHeight - bannerHeight)) {

            asideMenu.style.top = Math.round(windowOffsetTop - asideffsetTop) + margin + 'px';
        } else if (windowOffsetTop <= 0) {

            asideMenu.style.top = '0';
        }
    };

    /*
    ** SCROLL COLLECTION
    *******/
    Private.scrollCollection = function (event) {

        var href        = $(this).attr('href'),
            collections = $(href);

        $('html, body').animate({
            scrollTop : collections.offset().top + 'px'
        }, {
            duration    : 300,
            queue       : false
        });

        return false;
    };

    /*
    ** RENDERIZADOR DO COLLECTIONS
    *******/
    Private.renderCollections = function () {

        var tplCollections  = document.getElementById('collections-template'),
            tplList         = document.getElementById('list-template'),
            asideMenu       = document.getElementById('asideMenu').getElementsByTagName('ul')[0],
            content         = document.getElementsByClassName('collectionsRender')[0],
            htmlCollection  = '',
            htmlList        = '';

        if (content && content.getAttribute('data-init') === 'true') {

            Private.uri = content.getAttribute('data-uri');

            if (collectionsRender.collections.length > 0) {

                for (var index in collectionsRender.collections) {

                    htmlCollection  += Mustache.render(tplCollections.innerHTML.trim(), { layout : collectionsRender.layout,  collections : collectionsRender.collections[index]});
                    htmlList        += Mustache.render(tplList.innerHTML.trim(), collectionsRender.collections[index]);
                }
                
                content.innerHTML   = htmlCollection;
                asideMenu.innerHTML = htmlList;

                for (var index in collectionsRender.collections) {

                    /*
                    ** SHOW OFFERS
                    *******/
                    document.getElementsByClassName('moreOffers')[index].addEventListener('click', Private.showMoreOffers, false);

                    /*
                    ** OFFERS
                    *******/
                    Private.showMoreOffers({ layout : collectionsRender.layout,  collections : collectionsRender.collections[index]});
                }
            }
        }
    };

    /*
    ** SHOW MORE OFFERS
    *******/
    Private.showMoreOffers = function (obj) {

        var layout      = null,
            id          = null,
            query       = null,
            order       = null,
            page        = null,
            quantity    = null,
            columns     = null,
            queryString = '',
            timestamp   = new Date().getTime();

            Private.button = this;

        if (event) {

            layout      = this.getAttribute('data-layout');
            id          = this.getAttribute('data-id');
            query       = this.getAttribute('data-query');
            order       = this.getAttribute('data-order');
            page        = this.getAttribute('data-page');
            quantity    = this.getAttribute('data-quantity');
            columns     = this.getAttribute('data-columns');
        } else {

            layout      = obj.layout;
            id          = obj.collections.id;
            query       = obj.collections.query;
            order       = obj.collections.order;
            page        = obj.collections.page;
            quantity    = obj.collections.quantity;
            columns     = obj.collections.columns;
        }

        queryString = 
            'fq='           + query + id +
            '&O='           + order +
            '&PS='          + quantity +
            '&sl='          + layout +
            '&cc='          + columns +
            '&PageNumber='  + page +
            '&sm=1' +
            '&tt=';

        $.ajax({
            id          : id,
            url         : Private.uri + '/buscapagina?' + queryString + '&_=' + timestamp,
            type        : 'GET',
            dataType    : 'html',
            beforeSend  : Private.showMoreOffersBeforeSend,
            success     : Private.showMoreOffersSuccess
        });

        return false;
    };

    Private.showMoreOffersBeforeSend = function () {

        var button  = document.getElementById('button_' + this.id);
        
        button.classList.add('showLoader');
    };

    Private.showMoreOffersSuccess = function (obj) {

        var collection  = document.getElementById('collection_' + this.id),
            button      = document.getElementById('button_' + this.id),
            html        = document.createElement('html'),
            ul          = null,
            page        = 0;

        if (obj) {

            html.innerHTML = obj;

            page = parseInt(button.getAttribute('data-page'), 10);

            if (page <= 0) {

                collection
                    .getElementsByClassName('comboRender')[0]
                        .innerHTML = '';
            }

            page = (page + 1);

            ul = html.getElementsByTagName('ul');

            for (var index in ul) {
                
                if (ul[index].nodeType === 1) {

                    collection
                        .getElementsByClassName('comboRender')[0]
                            .appendChild(ul[index]);
                }
            }

            button.setAttribute('data-page', page);
        } else {

            button.style.display = 'none';
        }

        button.classList.remove('showLoader');
        
        return false;
    };

    return Public;

}(APP, {}, {}));