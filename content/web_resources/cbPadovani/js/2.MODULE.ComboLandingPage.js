/*
** COMBO LANDING PAGE
*******/
APP.Modules.ComboLandingPage2 = (function (app, Private, Public) {

    'use strict';

    Private = {
        button  : null
    },
    Public = {
        config : {
            ID      : 'ComboLandingPage',
            NAME    : 'ComboLandingPage',
            ENGAGE  : true
        }
    };

    Public.init = function () {

        /*
        ** SCROLL ASIDE
        *******/
        Private.scrollAside();

        /*
        ** SCROLL EVENT
        *******/
        document.addEventListener('scroll', Private.scrollAside, false);

        return true;
    };

    /*
    ** SCROLL ASIDE
    *******/
    Private.scrollAside = function (event) {

        var asideFixed      = document.getElementsByClassName('asideFixed')[0],
            asideBanner     = document.getElementsByClassName('asideBanner')[0],
            asideMenu       = document.getElementById('asideMenu'),
            asideffsetTop   = asideFixed.offsetTop,
            windowOffsetTop = window.pageYOffset;

        if (window.innerWidth >= 780) {

            if ((windowOffsetTop - 32) >= asideffsetTop) {

                asideBanner.classList.add('actived');
            } else if (windowOffsetTop <= 0) {

                asideBanner.classList.remove('actived');
            }
        } else {

            asideBanner.classList.remove('actived');
        }
    };

    return Public;

}(APP, {}, {}));